#ifndef KURSACH_ENCRYPTOR_H
#define KURSACH_ENCRYPTOR_H

#define ALPHABET "!! .,'ABCDEFHIGKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

#include <string>

class Encryptor {

private:
    int p;
    int q;
    int module;
    int euler_func;
    int e;
    int d;

    void init();

    int count_e();

    int count_d();

    static bool is_prime(int);

    static int get_alphabet_index(char let);

    char decrypt_char(char n);

public:
    int get_module() {
        return module;
    }

    int get_e() {
        return e;
    }

    Encryptor() {}

    Encryptor(int p, int q) : p(p), q(q) {
        init();
    }

    ~Encryptor() {
        delete &p;
        delete &q;
        delete &module;
        delete &euler_func;
        delete &e;
        delete &d;
    }

    std::string decrypt(std::string text);

    static long long recursive_power(long long, long long, int);
};


#endif //KURSACH_ENCRYPTOR_H
