#ifndef KURSACH_HASHER_H
#define KURSACH_HASHER_H

#include <string>

class Hasher {

private:
    int received;

public:
    void set_received(int val) {
        this->received = val;
    }

    int get_received() {
        return this->received;
    }

    static long hashSum(std::string str);
};


#endif //KURSACH_HASHER_H
