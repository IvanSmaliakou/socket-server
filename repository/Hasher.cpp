#include "headers/Hasher.h"

long Hasher::hashSum(std::string str) {
    int16_t hash = 0;
    for (std::string::size_type i = 0; i < str.size(); ++i) {
        hash += str[i];
    }
    return hash;
}
