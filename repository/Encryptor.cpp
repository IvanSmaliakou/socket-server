#include <vector>
#include <string>
#include "headers/Encryptor.h"

void Encryptor::init() {
    this->module = p * q;
    this->euler_func = (p - 1) * (q - 1);
    this->e = count_e();
    this->d = count_d();
}

int Encryptor::count_e() {
    std::vector<int> candidates;
    for (int i = 2; i <= euler_func / 2; ++i) {
        if (euler_func % i != 0) {
            if (is_prime(i)) {
                candidates.push_back(i);
            }
        }
    }
    return candidates[rand() % candidates.size()];
}

bool Encryptor::is_prime(int n) {
    for (int i = 2; i <= n / 2; ++i) {
        if (n % i == 0) return false;
    }
    return true;
}

int Encryptor::count_d() {
    std::vector<int> candidates;
    for (int i = 0; i < 10000000 && candidates.size() < 2; ++i) {
        int n = rand() % 1000;
        if ((n * e) % euler_func == 1) {
            candidates.push_back(n);
        }
    }
    return candidates[rand() % candidates.size()];
}

long long Encryptor::recursive_power(int64_t n, int64_t pow, int remainder) {
    if (pow == 1) {
        return n % remainder;
    }
    if (pow % 2 == 0) {
        return recursive_power(n * n % remainder, pow / 2, remainder) % remainder;
    }
    return n % remainder * recursive_power(n * n % remainder, pow / 2, remainder);
}

std::string Encryptor::decrypt(std::string text) {
    std::string decrypted;
    for (std::string::size_type i = 0; i < text.size(); ++i) {
        decrypted.push_back(decrypt_char(text[i]));
    }
    return decrypted;
}

char Encryptor::decrypt_char(char let) {
    int index = get_alphabet_index(let);
    int decrypted_index = recursive_power(index , d, module) % module;
    char ch = ALPHABET[decrypted_index];
    return ch;
}

int Encryptor::get_alphabet_index(char let) {
    for (int i = 0; i < strlen(ALPHABET); ++i) {
        char s = ALPHABET[i];
        if (ALPHABET[i] == let) {
            return i;
        }
    }
    return -1;
}
