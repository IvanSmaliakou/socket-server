#include <sys/socket.h>
#include <netinet/in.h>
#include <zconf.h>
#include <cstdio>
#include <vector>
#include <iostream>
#include <thread>

#include "headers/Router.h"
#include "../exceptions/headers/RoutingException.h"
#include "headers/AccumulateConnection.h"

#define MAX_CONN 5

int Router::ListenAndServe(unsigned short raw_port) {
    AccumulateConnection acc = AccumulateConnection(handler);
    std::vector<RoutingException> exception;
    std::string response;
    sockaddr remotehost_info;
    socklen_t remotehost_info_len;
    sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(raw_port);

    int sock_descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    int status_code = bind(sock_descriptor, reinterpret_cast<const sockaddr *>(&server_addr), sizeof(server_addr));
    if (status_code < 0) {
        close(sock_descriptor);
        throw new RoutingException("ERROR on bind()");
    }
    status_code = listen(sock_descriptor, MAX_CONN);
    if (status_code < 0) {
        close(sock_descriptor);
        throw new RoutingException("ERROR on listen()");
    }
    std::cout << "server started to accept new connections on port " << raw_port << std::endl;
    while (true) {
        int new_sock_descriptor = accept(sock_descriptor,
                                         &remotehost_info, &remotehost_info_len);
        if (new_sock_descriptor < 0) {
            close(sock_descriptor);
            close(new_sock_descriptor);
            throw new RoutingException("ERROR on accept");
        }
        std::thread thread(std::ref(acc), new_sock_descriptor, exception);
        thread.detach();
        if (!exception.empty()){
            throw new RoutingException(exception[0].what());
        }
    }

}