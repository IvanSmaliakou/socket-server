#include <sys/socket.h>
#include <netinet/in.h>
#include <zconf.h>
#include <cstdio>
#include <vector>
#include <iostream>

#ifndef KURSACH_ACCUMULATECONNECTION_H
#define KURSACH_ACCUMULATECONNECTION_H


class AccumulateConnection {
private:
    Handleable *handler;
public:
    AccumulateConnection(Handleable *arg) {
        handler = arg;
    }
    ~AccumulateConnection(){
        delete handler;
    }

    void operator()(int sock_descriptor, std::vector<RoutingException> exception) {
        std::mutex mu;
        try {
            char *request;
            int received;
            int status_code;
            std::string response;
            while (true) {
                request = new char[2048];
                response = "";
                received = recv(sock_descriptor, request, 100, 0);
                if (received < 0) {
                    throw new RoutingException("ERROR on receive message");
                }
                if (*request == '\0') {
                    break;
                }
                handler->Handle(request, response);
                status_code = send(sock_descriptor, response.c_str(), strlen(response.c_str()), 0);
                if (status_code == -1) {
                    throw new RoutingException("ERROR on send message");
                }
            }
        } catch (RoutingException ex) {
            std::lock_guard<std::mutex> guard(mu);
            exception.push_back(ex);
            return;
        }
    }
};


#endif //KURSACH_ACCUMULATECONNECTION_H
