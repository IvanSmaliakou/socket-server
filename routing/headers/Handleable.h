#ifndef KURSACH_HANDLEABLE_H
#define KURSACH_HANDLEABLE_H
#include <string>

class Handleable {
public:
    virtual ~Handleable(){}
    virtual void Handle(void *in, std::string &out) = 0;
};


#endif //KURSACH_HANDLEABLE_H
