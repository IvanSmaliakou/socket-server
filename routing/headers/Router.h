#ifndef KURSACH_ROUTER_H
#define KURSACH_ROUTER_H


#include "Handleable.h"

class Router {
private:
    Handleable* handler;
public:
    Router(Handleable* arg){
        handler = arg;
    }
    ~Router(){
        delete handler;
    }
    int ListenAndServe(unsigned short port);
};


#endif //KURSACH_ROUTER_H
