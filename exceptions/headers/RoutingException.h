#ifndef KURSACH_ROUTINGEXCEPTION_H
#define KURSACH_ROUTINGEXCEPTION_H


#include <exception>
#include <string>

class RoutingException : public std::exception {

    std::string cause;

public:
    RoutingException(std::string cause) {
        this->cause = cause;
    }

    ~RoutingException() {
        delete &cause;
    }

    const char *what() const throw() {
        return std::string("routing exception:").append(cause).c_str();
    }
};


#endif //KURSACH_ROUTINGEXCEPTION_H
