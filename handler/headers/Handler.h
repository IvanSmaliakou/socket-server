#ifndef KURSACH_HANDLER_H
#define KURSACH_HANDLER_H

#include "../../repository/headers/Encryptor.h"
#include "../../repository/headers/Hasher.h"
#include "../../routing/headers/Handleable.h"

class Handler : public Handleable {
private:
    Encryptor *encryptor;
    Hasher *hasher;

    std::string process(void *in);

public:
    Handler(Encryptor *e, Hasher *h) {
        this->encryptor = e;
        this->hasher = h;
    }

    ~Handler() {
        delete encryptor;
        delete hasher;
    }
    void Handle(void *in, std::string &out);
};


#endif //KURSACH_HANDLER_H
