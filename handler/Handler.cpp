#include <string>
#include <iostream>
#include "headers/Handler.h"
#include "../utils/headers/Serializer.h"
#include "../utils/headers/Utils.h"

void Handler::Handle(void *in, std::string &out) {
    out = process(in);
}

std::string Handler::process(void *ptr) {
    std::string req = static_cast<char *>(ptr);
    if (is_open_key_req(req)) {
        int e = encryptor->get_e();
        int module = encryptor->get_module();
        std::string serialized = Serializer::serialize_keys(module, e);
        return serialized;
    }
    if (is_hash_req(req)) {
        int hash = parse_hash_req(req);
        hasher->set_received(hash);
        return "";
    }
    if (is_data_req(req)) {
        std::string data = parse_data_request(req);
        std::string decrypted = encryptor->decrypt(data);
        std::cout<< "decrypted text is: "<< decrypted<< std::endl;
        int hash = hasher->hashSum(decrypted);
        if (hash == hasher->get_received()) {
            std::string msg = "passed";
            return msg;
        } else {
            std::string msg = "failed";
            return msg;
        }
    }
    return "";
}
