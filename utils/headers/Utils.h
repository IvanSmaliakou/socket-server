#ifndef KURSACH_UTILS_H
#define KURSACH_UTILS_H

bool is_hash_req(std::string text);

bool is_data_req(std::string basicString);

int parse_hash_req(std::string ptr);

std::string parse_data_request(std::string text);

bool is_open_key_req(std::string text);

int Init_p();

int Init_q();

#endif //KURSACH_UTILS_H
