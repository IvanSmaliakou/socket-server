#include <string>
#include <iostream>

bool is_open_key_req(std::string text) {
    return text == "openkey";
}

bool is_hash_req(std::string text) {
    return text.find("hash") != std::string::npos;
}

bool is_data_req(std::string text) {
    return text.find("data") != std::string::npos;
}

int parse_hash_req(std::string text) {
    int pos = text.find('=');
    std::string ret_str = text.substr(pos + 1, std::string::npos);
    return atoi(ret_str.c_str());
}

std::string parse_data_request(std::string text) {
    int pos = text.find('=');
    return text.substr(pos + 1, std::string::npos);
}


int Init_p() {
    int p;
    std::cout << "enter value of p: " << std::endl;
    std::cin >> p;
    return p;
}

int Init_q() {
    int q;
    std::cout << "enter value of q: " << std::endl;
    std::cin >> q;
    return q;
}