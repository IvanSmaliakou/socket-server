#include <string>
#include "headers/Serializer.h"

std::string Serializer::serialize_keys(int module, int e) {
    char buf [256];
    sprintf(buf, "module:{%d}, e:{%d}", module, e);
    return std::string(buf);
}
