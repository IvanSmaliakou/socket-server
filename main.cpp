#include <iostream>
#include <thread>
#include "routing/headers/Router.h"
#include "handler/headers/Handler.h"
#include "exceptions/headers/RoutingException.h"
#include "utils/headers/Utils.h"

int main() {
    Encryptor *encryptor = new Encryptor(Init_p(), Init_q());
    Hasher *hasher = new Hasher();
    Handler *handler = new Handler(encryptor, hasher);
    Router *r = new Router(handler);
    char *p = std::getenv("PORT");
    if (p == NULL) {
        p = "8080";
    }
    int port = atoi(p);

    try {
        r->ListenAndServe(port);
    } catch (RoutingException ex) {
        std::cerr << "Routing exception: " << ex.what() << std::endl;
    }
}

